from json import loads
import json
import requests
from grafana_api.grafana_face import GrafanaFace


grafana_api = GrafanaFace(
          auth='',
          host='monit-grafana.cern.ch',
          protocol='https'
          )


# get_all_production_uids = 'https://monit-grafana.cern.ch/api/search?folderIds=1285'
#
# with requests.Session() as s:
#     r = s.get(get_all_production_uids, headers=headers)
#     res = loads(r.text)

# print(res)

with open('colors_jsons/atlas_colour_codes.json') as json_file:
    colour_codes = json.load(json_file)


uid = 'p-ZuDUcWk'
#uid = '000000696'
dashboard_url = 'https://monit-grafana.cern.ch/api/dashboards/uid/{0}'.format(uid)

dashboard_json = grafana_api.dashboard.get_dashboard(uid)

for panel in dashboard_json['dashboard']['panels']:
    if 'aliasColors' in panel:
        panel['aliasColors'] = colour_codes['aliasColors']

grafana_api.dashboard.update_dashboard(dashboard={'dashboard': dashboard_json['dashboard'], 'overwrite': True})

print(dashboard_json)

